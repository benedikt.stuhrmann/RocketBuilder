﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class FollowFinger : MonoBehaviour {

    [SerializeField]
    private Hand side;
    private GameObject finger;

	// Use this for initialization
	void Start () {
        StartCoroutine("LateStart");
    } // end Start

    private IEnumerator LateStart() {
        yield return new WaitForSeconds(5);
        string name = (this.side == Hand.Left) ? "hands:b_l_index_ignore" : "hands:b_r_index_ignore";
        this.finger = GameObject.Find(name);
        if (this.finger != null) {
            this.transform.position = this.finger.transform.position;// + (dir * 0.01f);
            this.transform.rotation = this.finger.transform.rotation;
        } else {
            Debug.Log("Info: FollowFinger: No Finger(" + this.side + ") found");
        }
    } // end LateStart

    // Update is called once per frame
    void Update () {
        if (this.finger == null) return;
        /*
        OVRInput.Axis1D theFinger = (side == Hand.Left) ? OVRInput.Axis1D.PrimaryIndexTrigger : OVRInput.Axis1D.SecondaryIndexTrigger;
        
        // Zeigefinger ausgestreckt
        if ((OVRInput.Get(theFinger, OVRInput.Controller.Touch) > 0.1f)){
            this.GetComponent<Collider>().isTrigger = true;
        } else {
            this.GetComponent<Collider>().isTrigger = false;
        }*/

        Vector3 dir = (this.side == Hand.Left) ? -this.finger.transform.right : this.finger.transform.right;

        this.transform.position = this.finger.transform.position;// + (dir * 0.01f);
        this.transform.rotation = this.finger.transform.rotation;
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log(this.name + " Collider kollidiert mit " + other.name + " mit Tag " + other.tag);
        if (other.tag == "Button") {
            this.GetComponent<Collider>().isTrigger = false;
        }
    }

    private void OnTriggerExit(Collider other){
        if (other.tag == "Button") {
            this.GetComponent<Collider>().isTrigger = true;
        }
    }
}
