﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class SnappingPoint : MonoBehaviour {

    public SnapLocation snapLocation;
    [SerializeField]
    private List<RocketPart> snapsTo;
    private Snappable snappable;
    [SerializeField]
    private bool occupied = false;
    [SerializeField]
    private SnappingPoint connectedWith;

    void Start () {
        this.snappable = this.transform.parent.GetComponent<Snappable>();
    }

    void Update() {
        if (this.Occupied()) {
            float distance = (this.transform.position - this.connectedWith.transform.position).magnitude;
            float maxRadius = (this.GetComponent<SphereCollider>().radius * this.transform.localScale.y) + (this.connectedWith.GetComponent<SphereCollider>().radius * this.connectedWith.transform.localScale.y);

            if (distance > maxRadius) {
                this.snappable.OutOfReach(this, this.connectedWith);
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "SnappingPoint" && !this.Occupied() && !other.GetComponent<SnappingPoint>().Occupied())
            this.snappable.InReach(this, other.GetComponent<SnappingPoint>());
    } // end OnTriggerEnter

    private void OnTriggerExit(Collider other) {
        if (other.tag == "SnappingPoint") {
            this.snappable.OutOfReach(this, other.GetComponent<SnappingPoint>());
        }
    } // end OnTriggerExit

    public List<RocketPart> GetSnapsTo() {
        return this.snapsTo;
    } // end GetSnapsTo

    public bool Occupied() {
        return this.occupied;
    } // end Occupied

    public SnappingPoint ConnectedWith() {
        return this.connectedWith;
    } // end ConnectedWith

    public SnappingPoint ConnectedWith(SnappingPoint snappingPoint) {
        this.connectedWith = snappingPoint;
        this.occupied = (this.connectedWith != null);
        return this.connectedWith;
    } // end ConnectedWith

} // end SnappingPoint
