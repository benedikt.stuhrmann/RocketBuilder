﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Startbutton : MonoBehaviour {

	public Startzone startzone;
	private List<GameObject> rocketParts = new List<GameObject>();
	private bool wasClicked = false;
    [SerializeField]
    private Vector3 launchPadPos = Vector3.zero;
    [SerializeField]
    public float scaleFactor;
    public Text countdownText;

    private bool hallMoving = false;
    private float t = 0f;
    public float hallSpeed = 0.1f;
    GameObject hallRightSide;
	GameObject hallLeftSide;
	Vector3 defaultPositionRightSide;
	Vector3 defaultPositionLeftSide;
	Vector3 finalPositionRightSide;
	Vector3 finalPositionLeftSide;

    public AudioClip countdown;
    public AudioClip hallOpening;
    public AudioClip rocketSound;
    public AudioClip rocketSoundSidebooster;

    //AudioSource audioSource;

    void OnTriggerEnter(Collider other) {
        this.InitiateLaunch();
    }

    void OnMouseUp() {
        this.InitiateLaunch();
    }

    /*void Start() {
        this.audioSource = this.GetComponent<AudioSource>();
    }*/

	private void InitiateLaunch() {
        this.rocketParts = startzone.currentlyColliding;
        GameObject rocketModel = (this.rocketParts.Count == 0) ? null : this.rocketParts[0];

		if (!this.wasClicked && rocketModel != null) {
			foreach(Snappable snappableScript in rocketModel.GetComponentsInChildren<Snappable>()) {
				snappableScript.enabled = false;
			}
	        foreach (GameObject aRocketModel in this.rocketParts) {
	            // Größtes Raketenmodell wird genutzt, sollte es mehrere geben
	            if (aRocketModel.GetComponentsInChildren<Snappable>().Length > rocketModel.GetComponentsInChildren<Snappable>().Length) {
					rocketModel = aRocketModel;
	            }
	        }
            this.MoveHall();
			this.wasClicked = true;
			GameObject clonedRocketModel = Instantiate(rocketModel, this.launchPadPos, Quaternion.identity);
			clonedRocketModel.transform.localScale += new Vector3(this.scaleFactor, this.scaleFactor, this.scaleFactor);
            float rocketHeight = clonedRocketModel.GetComponent<CapsuleCollider>().height * this.scaleFactor;
            Transform topElement = clonedRocketModel.transform.GetChild(0);
            float topElementHeight = topElement.GetComponent<CapsuleCollider>().height * clonedRocketModel.transform.localScale.y * topElement.localScale.y;
            clonedRocketModel.transform.position = new Vector3(clonedRocketModel.transform.position.x - 46f, rocketHeight - (topElementHeight / 2) + 2f, clonedRocketModel.transform.position.z);
            clonedRocketModel.transform.rotation = Quaternion.Euler(0, 90, 0);
            clonedRocketModel.tag = "LaunchingRocket";
			foreach(Snappable snappableScript in clonedRocketModel.GetComponentsInChildren<Snappable>()) {
				snappableScript.enabled = false;
			}
			foreach(SnappingPoint snappingPointScript in clonedRocketModel.GetComponentsInChildren<SnappingPoint>()) {
				snappingPointScript.enabled = false;
			}

			StartCoroutine(Launch(clonedRocketModel));
            //this.audioSource.clip = this.countdown;
            //this.audioSource.Play();

        }


    } // end InitiateLaunch

	public void MoveHall() {
        //this.audioSource.clip = this.hallOpening;
        //this.audioSource.Play();
		hallRightSide = GameObject.Find("/Hall/RightSide");
		hallLeftSide = GameObject.Find("/Hall/LeftSide");
		defaultPositionRightSide = hallRightSide.transform.position;
		defaultPositionLeftSide = hallLeftSide.transform.position;
		finalPositionRightSide = new Vector3(0, 0, 15);
		finalPositionLeftSide = new Vector3(0, 0, -15);
		this.hallMoving = true;

		// hallRightSide.transform.Translate(0, 0, 15);
		// hallLeftSide.transform.Translate(0, 0, -15);




		// hallRightSide.transform.position = Vector3.Lerp(hallRightSide.transform.position, hallRightSide.transform.position + new Vector3(0,0,10), 5);
		// hallLeftSide.transform.position = Vector3.Lerp(hallLeftSide.transform.position, hallLeftSide.transform.position + new Vector3(0,0,-10), 5);

		/*for(int i = 0; i<100; i++) {
			float hallMovementSpeed = 0.1f;
			hallRightSide.transform.position.z += hallMovementSpeed;
			hallLeftSide.transform.position.z -= hallMovementSpeed;
		}*/
	}

	void Update() {
		if (this.hallMoving) {
            this.t += Time.deltaTime * this.hallSpeed;
            hallRightSide.transform.position = Vector3.Lerp(defaultPositionRightSide, defaultPositionRightSide + finalPositionRightSide, this.t);
            hallLeftSide.transform.position = Vector3.Lerp(defaultPositionLeftSide, defaultPositionLeftSide + finalPositionLeftSide, this.t);
        }
        if (this.t >= 1f && this.hallMoving) {
            this.hallMoving = false;
            this.t = 0f;
        }
	}

	IEnumerator Launch(GameObject oldRocket) {

		GameObject rocket = GameObject.FindGameObjectWithTag ("LaunchingRocket");
		AudioSource audioSourceRocket = rocket.gameObject.AddComponent<AudioSource> ();
        audioSourceRocket.clip = this.rocketSoundSidebooster;
        audioSourceRocket.spatialize = true;

		float duration = 0f; // 3 seconds you can change this to
		//to whatever you want
		float totalTime = 10f;
		while(totalTime >= duration) {
			// countdownImage.fillAmount = totalTime / duration;
			totalTime -= Time.deltaTime;
			var integer = (int)totalTime; /* choose how to quantize this */
			/* convert integer to string and assign to text */
			// Debug.Log(integer);
			//this.countdownText.text = integer.ToString();

			if (integer == 3)
                audioSourceRocket.Play();
			
			yield return null;
		}
		float rate = 0.0005f;
		float speed = 0f;
		float maxSpeed = 10f;

        foreach (ParticleSystem system in rocket.GetComponentsInChildren<ParticleSystem>()) {
            system.Play();
        }


		for(float i = 0f; i < 50000; i++) {
			speed = rate*i/4;
			if(speed > maxSpeed)
				speed = maxSpeed;
			rocket.transform.position = new Vector3(rocket.transform.position.x, rocket.transform.position.y + speed, rocket.transform.position.z);
			yield return null;
		}
	}
}
