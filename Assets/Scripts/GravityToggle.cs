﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityToggle : MonoBehaviour {

    [SerializeField]
    private bool gravity = false;
    private bool block = false;
    [SerializeField]
    private Material mat_gravityOn;
    [SerializeField]
    private Material mat_gravityOff;
    [SerializeField]
    private Color color_gravityOn;
    [SerializeField]
    private Color color_gravityOff;
    private Transform statusLight;
    private Collider buttonCollider;
    private Vector3 defaultPos;

    void Start() {
        foreach(Transform child in this.transform.parent.GetComponentInChildren<Transform>()) {
            if (child.name == "LED") {
                this.statusLight = child;
            }
        }
        this.buttonCollider = this.GetComponent<Collider>();
        this.defaultPos = this.transform.position;
        this.UpdateStatusLight();
    } // end Start

    void Update() {
        if (this.buttonCollider.enabled == false) {
            if ((this.transform.position - this.defaultPos).magnitude <= 0.005f) {
                this.buttonCollider.enabled = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "TriggerGravity") {
            this.ToggleGravity();
            this.buttonCollider.enabled = false;
            this.block = true;
        }
    } // end OnTriggerEnter

    private void OnTriggerExit(Collider other) {
        if (other.tag == "TriggerGravity") {
            this.block = false;
        }
    } // end OnTriggerExit

    // Für MAUS
    void OnMouseDown() {
        this.ToggleGravity();
    } // end OnMouseDown

    /// <summary>
    /// Aktiviert bzw. deaktiviert die Gravitation für Raketenteile
    /// </summary>
    private void ToggleGravity() {
        this.gravity = !this.gravity;
        this.UpdateStatusLight();

        foreach (GameObject rocketPart in GameObject.FindGameObjectsWithTag("RocketPart")) {
            // Raketenteile mit Parent werden nicht beeinflusst
            if (rocketPart.transform.parent != null) continue;

            Rigidbody rigid = rocketPart.GetComponent<Rigidbody>();
            rigid.isKinematic = !this.gravity;
            rigid.useGravity = this.gravity;
        }

        foreach (GameObject rocketPart in GameObject.FindGameObjectsWithTag("RocketModel")) {
            Rigidbody rigid = rocketPart.GetComponent<Rigidbody>();
            rigid.isKinematic = !this.gravity;
            rigid.useGravity = this.gravity;
        }
    } // end ToggleGravity

    private void UpdateStatusLight() {
        this.statusLight.GetComponent<Renderer>().material = (this.gravity) ? this.mat_gravityOn : this.mat_gravityOff;
        this.statusLight.GetComponentInChildren<Light>().color = (this.gravity) ? this.color_gravityOn : this.color_gravityOff;
    } // end UpdateStatusLight

} // end GravityToggle