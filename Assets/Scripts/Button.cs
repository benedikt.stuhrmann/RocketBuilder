﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour {

    private Vector3 defaultPos;
    public Vector3 pressedPos = new Vector3(0f, 0f, -0.01f);
    private bool movingIn = false;
    private bool movingOut = false;
    private float t = 0f;
    public float speed = 5f;
    public bool canBePressed = true;

    void Start() {
        this.defaultPos = this.transform.position;    
    }

    void Update() {
        if (this.movingIn) {
            this.t += Time.deltaTime * this.speed;
            this.transform.position = Vector3.Lerp(this.defaultPos, this.defaultPos + this.pressedPos, this.t);
        } else if (this.movingOut) {
            this.t += Time.deltaTime * this.speed;
            this.transform.position = Vector3.Lerp(this.defaultPos + this.pressedPos, this.defaultPos, this.t);
        }

        if (this.t >= 1f && this.movingIn) {
            this.movingIn = false;
            this.movingOut = true;
            this.t = 0f;
        } else if (this.t >= 1f && this.movingOut) {
            this.movingIn = false;
            this.movingOut = false;
            this.canBePressed = true;
        }
    }

    void OnTriggerEnter(Collider other) {
        this.Move();
    }

    private void OnMouseUp() {
        this.Move();
    }

    private void Move() {
        if (this.canBePressed) {
            this.movingIn = true;
            this.t = 0f;
            this.canBePressed = false;
        }
    }

}
