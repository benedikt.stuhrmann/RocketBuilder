﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButton : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        this.Restart();
    }

    private void OnMouseUp() {
        this.Restart();
    }

    private void Restart() {
        if (this.GetComponent<Button>().canBePressed) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
