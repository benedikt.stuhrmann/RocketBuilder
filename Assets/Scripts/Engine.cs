﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour {

	// weight in kg
	public int weight = 10000;

	// thrust in kg
	public int thrust = 50000;

	// fuel consumption per second in liters
	public int fuelConsumptionPerSecond = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
