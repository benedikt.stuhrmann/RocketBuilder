﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoDisplay : MonoBehaviour {

    [SerializeField]
    private string title = "";
    [SerializeField]
    private string subtitle = "";
    [SerializeField]
    private string mass = "";
    [SerializeField]
    private string description = "";

    [SerializeField]
    private float displayDistance = 0.2f;
    [SerializeField]
    private Vector3 displayOffset = Vector3.zero;

    private bool active = false;

    public void SetText(Canvas canvas) {
        foreach (Transform child in canvas.GetComponentsInChildren<Transform>()) {
            if (child.name == "Title") child.GetComponent<Text>().text = this.title;
            if (child.name == "Subtitle") child.GetComponent<Text>().text = this.subtitle;
            if (child.name == "Mass") child.GetComponent<Text>().text = (this.mass != "") ? "Mass: " + this.mass : "";
            if (child.name == "Description") child.GetComponent<Text>().text = this.description.Replace("<br>", "\n");

            this.active = true;
        }
    } // end SetText

    public void Activate(bool active) {
        this.active = active;
    } // end Activate

    public float GetDisplayDistance() {
        return -this.displayDistance;
    } // end GetDisplayDistance

    public Vector3 GetDisplayOffset() {
        return this.displayOffset;
    } // end GetDisplayOffset

} // end InfoDisplay
