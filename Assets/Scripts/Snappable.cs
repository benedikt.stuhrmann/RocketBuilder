﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Snappable : MonoBehaviour {

    public RocketPart type;
    private Hashtable snappingPoints = new Hashtable();
    private Vector3 prevPos;
    private bool triggersEnabled = false;
    private Transform model;
    [SerializeField]
    private float minMove = 0.01f;
    public Transform snapPreview;

    public SnappingPoint activeSnappingPoint;
    public SnappingPoint otherActiveSnappingPoint;

    private bool doBlockInReach = false;

    private bool controllersConnected = false;
    private SphereCollider rightHand;
    private SphereCollider leftHand;

    public AudioSource snapSound;

    [SerializeField]
    private GameObject rocketModelPrefab;
    public bool useRocketModel = true;

    void Start () {
        this.prevPos = this.transform.position;

        // Referenzen für SnappingPoints und das 3D-Model für spätere Verwendung speichern
        Transform[] children = this.GetComponentsInChildren<Transform>();
        foreach (Transform child in children) {
            if (child.tag == "SnappingPoint") {
                SnappingPoint snappingPoint = child.GetComponent<SnappingPoint>();
                snappingPoint.GetComponent<Collider>().isTrigger = false;
                this.snappingPoints[snappingPoint] = snappingPoint.GetSnapsTo();
            }
            if (child.name == "Model") {
                this.model = child;
            }
        }

        // Referenzen auf die beiden "Hände" speichern
        this.rightHand = GameObject.Find("InHandRight").GetComponent<SphereCollider>();
        this.leftHand = GameObject.Find("InHandLeft").GetComponent<SphereCollider>();

        // beide Controller verbunden?
        this.controllersConnected = (OVRInput.IsControllerConnected(OVRInput.Controller.LTouch) && OVRInput.IsControllerConnected(OVRInput.Controller.RTouch));
    } // end Start
    
    void Update () {
        // Zurückgelegte Entfernung seit dem letzten Aufruf von Update
        float movedDistance = this.GetMovedDistance();

        if (this.controllersConnected && OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.Touch) == 0f && this.activeSnappingPoint != null) {
            // Wenn linker Controller angeschlossen ist und Hand geöffnet ist
            // checken ob aktiver SnappingPoint (wenn existent) zu Objekt in linker Hand gehört
            if (this.leftHand.bounds.Intersects(this.activeSnappingPoint.transform.parent.GetComponent<Collider>().bounds)) {
                this.Snap ();
            }
        }

        if (this.controllersConnected && OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger, OVRInput.Controller.Touch) == 0f && this.activeSnappingPoint != null) {
            // Wenn rechter Controller angeschlossen ist und Hand geöffnet ist
            // checken ob aktiver SnappingPoint zu Objekt in rechter Hand gehört
            if (this.rightHand.bounds.Intersects(this.activeSnappingPoint.transform.parent.GetComponent<Collider>().bounds)) {
                this.Snap ();
            }
        }

        if (!this.triggersEnabled && movedDistance > this.minMove) {
            // Trigger aktivieren, wenn Objekt bewegt wird
            this.doBlockInReach = false;
            this.ToggleTriggers(true);
        }
        else if (this.triggersEnabled && movedDistance == 0) {
            // Trigger deaktivieren, wenn Objekt nicht bewegt wird
            this.ToggleTriggers(false);
        }

        this.prevPos = this.transform.position;
    } // end Update

    /// <summary>
    /// Schaltet Trigger der SnappingPoints an bzw. aus.
    /// </summary>
    /// <param name="enable">true, trigger werden eingeschaltet. false, trigger werden ausgeschaltet</param>
    public void ToggleTriggers(bool enable) {
        this.triggersEnabled = enable;
        foreach (SnappingPoint snappingPoint in this.snappingPoints.Keys) {
            //if (!snappingPoint.Occupied()) {
                snappingPoint.GetComponent<Collider>().isTrigger = this.triggersEnabled;
            //}
        }
    } // end ToggleTriggers

    /// <summary>
    /// Von den SnappingPoints (Kindern) aus aufgerufen, wenn Collider getriggert wird.
    /// </summary>
    /// <param name="snappingPoint">Der SnappingPoint, von welchem die Nachricht kommt.</param>
    /// <param name="otherSnappingPoint">Der andere SnappingPoint, welcher den Trigger ausgelöst hat.</param>
    public void InReach(SnappingPoint snappingPoint, SnappingPoint otherSnappingPoint) {
        // Debug.Log(snappingPoint.name + " (" + snappingPoint.Occupied() + ") " + " of " + snappingPoint.transform.parent.name + " in reach of " + otherSnappingPoint.name + " (" + otherSnappingPoint.Occupied() + ") " + " of " + otherSnappingPoint.transform.parent.name);
        if (this.doBlockInReach || snappingPoint.Occupied() || otherSnappingPoint.Occupied()) return;

        Snappable otherSnappable = otherSnappingPoint.transform.parent.GetComponent<Snappable>();
        
        // Testen ob Verbindung erlaubt
        bool snapAllowed = false;
        List<RocketPart> allowedSnaps = snappingPoint.GetSnapsTo();
        List<RocketPart> otherAllowedSnaps = otherSnappingPoint.GetSnapsTo();
        // darf ich ransnappen? UND der andere auch?
        if (otherAllowedSnaps.Contains(this.type) && allowedSnaps.Contains(otherSnappable.type)) {
            snapAllowed = true;
        }

        // Snapping nicht erlaubt => abbrechen
        if (!snapAllowed || otherSnappable.type == RocketPart.Booster) return;

        // snappingPoint (Preview) an otherSnappingPoint ransnappen
        this.snapPreview.position = otherSnappingPoint.transform.position;
        this.snapPreview.rotation = otherSnappingPoint.transform.rotation;

        // otherSnappable zum Parent machen
        this.snapPreview.SetParent(otherSnappable.transform);

        // um halbe Höhe nach oben/unten verschieben
        CapsuleCollider coll = this.GetComponent<CapsuleCollider>();
        if (otherSnappingPoint.snapLocation == SnapLocation.Bottom)
            this.snapPreview.localPosition = new Vector3(this.snapPreview.localPosition.x, this.snapPreview.localPosition.y - (coll.height / 2), this.snapPreview.localPosition.z);
        if (otherSnappingPoint.snapLocation == SnapLocation.Top)
            this.snapPreview.localPosition = new Vector3(this.snapPreview.localPosition.x, this.snapPreview.localPosition.y + (coll.height / 2), this.snapPreview.localPosition.z);
        if (otherSnappingPoint.snapLocation == SnapLocation.Side && otherSnappingPoint.transform.localPosition.x > 0) {
            this.snapPreview.localRotation = Quaternion.Euler(0, 180, 0);
        }

        this.snapPreview.gameObject.SetActive(true);

        // Referenz für die beiden SnappingPoints speichern
        this.activeSnappingPoint = snappingPoint;
        this.otherActiveSnappingPoint = otherSnappingPoint;
    } // end InReach

    /// <summary>
    /// Von den SnappingPoints (Kindern) aus aufgerufen, wenn Collider getriggert wird.
    /// </summary>
    /// <param name="snappingPoint">Der SnappingPoint, von welchem die Nachricht kommt.</param>
    /// <param name="otherSnappingPoint">Der andere SnappingPoint, welcher den Trigger ausgelöst hat.</param>
    public void OutOfReach(SnappingPoint snappingPoint, SnappingPoint otherSnappingPoint) {
        // wirklich außerhalb des Triggers?
        float distance = (snappingPoint.transform.position - otherSnappingPoint.transform.position).magnitude;
        float maxRadius = (snappingPoint.GetComponent<SphereCollider>().radius * snappingPoint.transform.localScale.y) + (otherSnappingPoint.GetComponent<SphereCollider>().radius * otherSnappingPoint.transform.localScale.y);

        if (distance > maxRadius) {
            Transform otherSnappable = otherSnappingPoint.transform.parent;

            // Wenn other Kind- oder Elternelement ist => Verbindung auflösen
            if (otherSnappable.parent == this.transform) {
                otherSnappable.transform.SetParent(null);
                this.UpdateRocketModel();
                otherSnappable.GetComponent<Snappable>().UpdateRocketModel();
            } else if (otherSnappable == this.transform.parent) {
                this.transform.SetParent(null);
                this.UpdateRocketModel();
                otherSnappable.GetComponent<Snappable>().UpdateRocketModel();
            }

            if (snappingPoint.ConnectedWith() == otherSnappingPoint) snappingPoint.ConnectedWith(null);
            if (otherSnappingPoint.ConnectedWith() == snappingPoint) otherSnappingPoint.ConnectedWith(null);

            // SnappingPreview entfernen
            this.ResetSnapping();
        }
    } // end OutOfReach

    /// <summary>
    /// Wird automatisch aufgerufen, wenn ein Objekt losgelassen wird und
    /// snappt das Objekt an das jeweilige andere Objekt, sofern eines in Reichweite ist.
    /// </summary>
    public void Snap(bool passed = false) {
        if (this.otherActiveSnappingPoint != null && this.otherActiveSnappingPoint.snapLocation == SnapLocation.Side) {
            if (this.type == RocketPart.Booster) this.SnapSide();
            return;
        }


        // Debug.Log("Snap of " + this.name);
        if (this.activeSnappingPoint == null || this.otherActiveSnappingPoint == null) {
            // nicht selbst, aber vlt. bei einem der Kinder?
            foreach (Snappable child in this.GetComponentsInChildren<Snappable>()) {
                if (child.activeSnappingPoint != null && child.otherActiveSnappingPoint != null) {
                    // Debug.Log("Passing to child");
                    child.Snap(true);
                }
            }

            // Debug.Log("Returning from " + this.name);
            if (this.transform.parent != null && this.transform.parent.tag != "RocketModel") this.transform.SetParent(null);
            return;

        }
        // Debug.Log("Continue in " + this.name);

        this.ToggleTriggers(false);

        Transform otherSnappable = this.otherActiveSnappingPoint.transform.parent;

        // Elternelemente speichern
        Snappable[] parents = this.GetComponentsInParent<Snappable>();
        Transform[] connectedRocketParts = new Transform[parents.Length];
        for (int i = 0; i < parents.Length; i++) {
            parents[i].ToggleTriggers(false);
            connectedRocketParts[i] = parents[i].GetComponent<Transform>();
        }

        Transform topElement = connectedRocketParts[connectedRocketParts.Length - 1];
        //Debug.Log("TopElement: " + topElement.name + ", in Snap() von: " + this.name);

        // Snappable an otherSnappingPoint ransnappen
        topElement.position = this.otherActiveSnappingPoint.transform.position;
        topElement.rotation = this.otherActiveSnappingPoint.transform.rotation;

        // otherSnappable zum Parent machen
        Transform savedParent = topElement.parent;
        topElement.SetParent(otherSnappable.transform);

        // um halbe Höhe nach oben/unten verschieben
        float myHeight = topElement.GetComponent<CapsuleCollider>().height;
        float height = 0;
        foreach(Transform element in connectedRocketParts) {
            height += element.GetComponent<CapsuleCollider>().height;
        }
        float heightOffset = height - (myHeight / 2);
        
        if (this.otherActiveSnappingPoint.snapLocation == SnapLocation.Bottom)
            topElement.localPosition = new Vector3(topElement.localPosition.x, topElement.localPosition.y - heightOffset, topElement.localPosition.z);
        if (this.otherActiveSnappingPoint.snapLocation == SnapLocation.Top)
            topElement.localPosition = new Vector3(topElement.localPosition.x, topElement.localPosition.y + heightOffset, topElement.localPosition.z);

        topElement.SetParent(savedParent);

        // dann richtig Einordnen (als Kind oder Parent)
        if (this.otherActiveSnappingPoint.snapLocation == SnapLocation.Bottom) {
            // ich werde zum Kind vom otherSnappingPoint
            if (otherSnappable.parent == this.transform) otherSnappable.SetParent(null);
            this.transform.SetParent(otherSnappable.transform);
        } else {
            // ich werde zum Parent vom otherSnappingPoint
            if (this.transform.parent == otherSnappable) this.transform.SetParent(null);
            otherSnappable.SetParent(this.transform);
        }

        // betroffene SnappingPoints als belegt markieren
        this.activeSnappingPoint.ConnectedWith(otherActiveSnappingPoint);
        this.otherActiveSnappingPoint.ConnectedWith(activeSnappingPoint);

        // play snap sound one time
        if (this.snapSound != null) this.snapSound.Play();

        // add this element to rocket parts of start logic
        // BroadcastMessage("AddMeToRocketParts", this);
        // BroadcastMessage("TestMe");

        this.ResetSnapping();
    } // end Snap()

    private void SnapSide() {
        // snappingPoint an otherSnappingPoint ransnappen
        this.transform.position = this.otherActiveSnappingPoint.transform.position;
        this.transform.rotation = this.otherActiveSnappingPoint.transform.rotation;

        // otherSnappable zum Parent machen
        this.transform.SetParent(this.otherActiveSnappingPoint.transform.parent);

        if (this.otherActiveSnappingPoint.transform.localPosition.x > 0) {
            this.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        // betroffene SnappingPoints als belegt markieren
        this.activeSnappingPoint.ConnectedWith(otherActiveSnappingPoint);
        this.otherActiveSnappingPoint.ConnectedWith(activeSnappingPoint);

        // play snap sound one time
        if (this.snapSound != null) this.snapSound.Play();

        this.ResetSnapping();
    } // end SnapSide

    /// <summary>
    /// Bewegte Distanz, seit dem letzten LateUpdate
    /// </summary>
    /// <returns>Bewegte Distanz, seit dem letzten LateUpdate</returns>
    private float GetMovedDistance() {
        return (this.transform.position - this.prevPos).magnitude;
    } // end GetMovedDistance

    private void ResetSnapping() {
        // SnapPreview wird wieder Kind des Snappables
        this.snapPreview.parent = this.transform;
        // SnapPreview deaktivieren
        this.snapPreview.gameObject.SetActive(false);
        // RocketModel updaten
        this.UpdateRocketModel();
        // Aktive SnappingPoints zurücksetzen
        this.activeSnappingPoint = null;
        this.otherActiveSnappingPoint = null;
        // InReach kurzzeitig blockieren, um komisches Snapping zu vermeiden
        this.doBlockInReach = true;
    } // end ResetSnapping

    private void UpdateRocketModel() {
        if (!this.useRocketModel) return;

        // wenn alleine stehendes ELement => RocketModel entfernen wenn vorhanden
        // (keine Kinder, aber RocketModel als parent)
        if (this.GetComponentsInChildren<Snappable>().Length == 1 && this.transform.parent != null && this.transform.parent.tag == "RocketModel") {
            GameObject theParent = this.transform.parent.gameObject; // das RocketModel
            this.transform.SetParent(null);
            //if(GameObject.Find)
            Destroy(theParent);
            return;
        }

        // keine Kinder, und auch kein RocketModel als parent => abbrechen
        if (this.GetComponentsInChildren<Snappable>().Length == 1 && this.transform.parent == null) return;

        // sonst (wenn Kinder) ein Rocketmodel als obersten Parent einfügen
        Transform topElement = this.transform;

        while(topElement.parent != null) {
            topElement = topElement.parent;
        }

        Transform rocketModel;

        Debug.Log ("topElemtn Tag: " + topElement.tag);

        if (!topElement.CompareTag("RocketModel") && !topElement.CompareTag("LaunchingRocket")) {
            // Wenn das oberste Element kein RocketModel ist, eins einfügen

            // RocketModel erstellen und als parent setzen
            rocketModel = Instantiate(this.rocketModelPrefab).GetComponent<Transform>();
        } else {
            // Wenn das oberste Element bereits ein RocketModel ist, dieses updaten
            rocketModel = topElement;
            topElement = topElement.GetComponentInChildren<Snappable>().transform;
            topElement.SetParent(null);
        }


        // RocketModel aktualisieren
        rocketModel.transform.position = topElement.transform.position;
        rocketModel.transform.rotation = topElement.transform.rotation;
        topElement.SetParent(rocketModel);

        // Collider von RocketModel anpassen, dass er so groß wie die ganze Rakete ist
        float rocketHeight = 0f;
        float prevScale = 1f;
        foreach(Snappable part in rocketModel.GetComponentsInChildren<Snappable>()) {
            if (part.type == RocketPart.Booster) continue;
            prevScale = (part.transform.localScale.y * prevScale);
            rocketHeight += (part.GetComponent<CapsuleCollider>().height * prevScale);
        }
        CapsuleCollider rocketModelCollider = rocketModel.GetComponent<CapsuleCollider>();
        rocketModelCollider.height = rocketHeight;
        float topElementHeight = topElement.GetComponent<CapsuleCollider>().height * (1 / rocketModel.localScale.y);
        float yCenter = - (rocketHeight / 2) + (topElementHeight / 2);
        rocketModelCollider.center = new Vector3(0, yCenter, 0);

        Transform tempElement = topElement;
        //while (tempElement.childCount > 0) {
            //if(tempElement != topElement)
                //tempElement.GetComponent<CapsuleCollider> ().enabled = false;
            //tempElement = tempElement.GetChild (0);
            foreach (CapsuleCollider collider in tempElement.GetComponentsInChildren<CapsuleCollider>()) {
                collider.enabled = false;
            }
        //}

        // Alle leeren RocketModels löschen
        foreach(GameObject model in GameObject.FindGameObjectsWithTag("RocketModel")) {
            if (model.transform.childCount == 0) Destroy(model);
        }
    } // end UpdateRocketModel

} // end Snappable
