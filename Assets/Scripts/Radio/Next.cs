﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Next : MonoBehaviour {

	public GameObject radio;
	public AudioSource clickSound;

	void OnMouseDown() {
        if (this.clickSound != null) this.clickSound.Play();
	}

	void OnMouseUp() {
		// Debug.Log("MouseUp Next");
		this.radio.SendMessage("ButtonClicked", "Next");
	}

    void OnTriggerEnter(Collider other) {
        if (this.clickSound != null) this.clickSound.Play();
		this.radio.SendMessage("ButtonClicked", "Next");
    }
}
