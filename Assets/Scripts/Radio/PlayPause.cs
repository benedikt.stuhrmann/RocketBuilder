﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPause : MonoBehaviour {

	public GameObject radio;
	public AudioSource clickSound;

	void OnMouseDown() {
        if (this.clickSound != null) this.clickSound.Play();
	}

	void OnMouseUp() {
		// Debug.Log("MouseUp PlayPause");
		this.radio.SendMessage("ButtonClicked", "PlayPause");
	}

    void OnTriggerEnter(Collider other) {
        if (this.clickSound != null) this.clickSound.Play();
		this.radio.SendMessage("ButtonClicked", "PlayPause");
    }
}
