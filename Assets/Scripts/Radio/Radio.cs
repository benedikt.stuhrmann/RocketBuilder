﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour {

	public GameObject buttonPrev;
	public GameObject buttonPlayPause;
	public GameObject buttonNext;


	public AudioClip song1;
	public AudioClip song2;
	public AudioClip song3;
	public AudioSource audio;

    public bool autoPlay = true;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		audio.clip = song1;

        if (this.autoPlay) {
            this.audio.Play();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ButtonClicked(string nameOfButton) {
		switch(nameOfButton) {
			case "Prev":
				if(audio.clip == song1)
					audio.clip = song3;
				else if(audio.clip == song2)
					audio.clip = song1;
				else if(audio.clip == song3)
						audio.clip = song2;
				audio.Play();
				break;
			case "PlayPause":
				if(!audio.isPlaying)
					audio.Play();
				else
					audio.Pause();
				break;
			case "Next":
				if(audio.clip == song1)
					audio.clip = song2;
				else if(audio.clip == song2)
					audio.clip = song3;
				else if(audio.clip == song3)
						audio.clip = song1;
				audio.Play();
				break;
			default: break;
		}
	}
	
}
