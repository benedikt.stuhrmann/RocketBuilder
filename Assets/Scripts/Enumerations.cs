﻿namespace Enums {

    public enum RocketPart : int {
        FuelTank,
        Engine,
        Nose,
        Booster
    } // end RocketPart

    public enum SnapLocation : int {
        Top,
        Bottom,
        Side
    } // end SnapLocation

    public enum Hand : int {
        Left,
        Right
    } // end Hand

} // Enums