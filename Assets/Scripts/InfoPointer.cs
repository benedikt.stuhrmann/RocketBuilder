﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class InfoPointer : MonoBehaviour {

    [SerializeField]
    private Hand side;
    [SerializeField]
    private Transform head;
    private LineRenderer line;
    [SerializeField]
    private Canvas infoDisplayPrefab;
    private InfoDisplay infoDisplay;
    private Canvas activeCanvas;

    public Transform indexFinger;

    void Start() {
        this.line = this.GetComponent<LineRenderer>();
        StartCoroutine("LateStart");
    }

    private IEnumerator LateStart() {
        yield return new WaitForSeconds(5);
        string name = (this.side == Hand.Left) ? "hands:b_l_index_ignore" : "hands:b_r_index_ignore";
        GameObject finger = GameObject.Find(name);
        if (finger != null) {
            this.indexFinger = finger.GetComponent<Transform>();
        } else {
            Debug.Log("Info: InfoPointer: No IndexFinger(" + this.side + ") found");
        }
    }

    void Update () {

        if (this.indexFinger == null) return;

        // Abfrage ob Zeigefinger ausgestreckt ist und hold button aktiv ist
        // nur dann raycasten etc
        // sonst aktivesDisplay löschen
        if (this.IndexFingerPointing(this.side)) {
            this.line.enabled = true;
            RaycastHit hit;
            Vector3 dir = (this.side == Hand.Left) ? -this.indexFinger.right : this.indexFinger.right;
            Ray ray = new Ray(this.indexFinger.position, dir);

            Debug.DrawRay(ray.origin, ray.direction * 20, Color.blue);
            this.line.SetPosition(0, ray.origin);

            if (Physics.Raycast(ray, out hit)) {
                this.line.SetPosition(1, hit.point);

                // Rocket Part oder LaunchZone oder LaunchButton?
                if (hit.collider.tag == "RocketPart" || hit.collider.tag == "LaunchZone" || hit.collider.tag == "LaunchButton") {
                    // wenn ja, prüfen ob bereits Display besteht
                    if (this.activeCanvas && this.infoDisplay) {
                        // wenn bereits eines besteht, prüfen ob es zum Objekt gehört, auf das jetzt gezeigt wird
                        if (hit.collider.name != this.infoDisplay.name) {
                            // ist ein anderes Objekt als gerade angezeigt wird => löschen und neues Display erstellen
                            this.DestroyActiveDisplay();
                            this.CreateNewDisplay(ray, hit);
                        } else {
                            this.PositionDisplay();
                        }
                    }
                    else {
                        // wenn keins besteht, eines erstellen
                        this.CreateNewDisplay(ray, hit);
                    }
                }
                else if (this.infoDisplay != null) {
                    // wenn nicht, dann mögliches aktives Display entfernen
                    this.DestroyActiveDisplay();
                }
            }
        } else {
            this.line.enabled = false;
            this.DestroyActiveDisplay();
        }
    } // end Update

    private void CreateNewDisplay(Ray ray, RaycastHit hit) {
        this.infoDisplay = hit.transform.GetComponent<InfoDisplay>();

        this.activeCanvas = Instantiate(this.infoDisplayPrefab);
        this.infoDisplay.SetText(this.activeCanvas);

        this.PositionDisplay();
    } // end CreateNewDisplay

    private void PositionDisplay() {
        Vector3 dir = this.infoDisplay.transform.position - this.head.position;
        Vector3 pos = this.infoDisplay.transform.position + (dir * this.infoDisplay.GetDisplayDistance());
        pos += this.infoDisplay.GetDisplayOffset();
        Quaternion rot = Quaternion.LookRotation(dir, Vector3.up);

        this.activeCanvas.transform.position = pos;
        this.activeCanvas.transform.rotation = rot;
    } // end PositionDisplay

    private void DestroyActiveDisplay() {
        if (this.infoDisplay != null) {
            this.infoDisplay.Activate(false);
            this.infoDisplay = null;
        }
        if (this.activeCanvas != null) {
            Destroy(this.activeCanvas.gameObject);
        }
    } // end DestroyActiveDisplay

    /// <summary>
    /// Überprüft ob angegebene Hand geschlossen ist und der Zeigefinger ausgestreckt ist.
    /// </summary>
    /// <param name="side">Die Hand, welche getestet werden soll</param>
    /// <returns>true, false</returns>
    private bool IndexFingerPointing(Hand side) {
        OVRInput.Axis1D theHand = (side == Hand.Left) ? OVRInput.Axis1D.PrimaryHandTrigger : OVRInput.Axis1D.SecondaryHandTrigger;
        OVRInput.Axis1D theFinger = (side == Hand.Left) ? OVRInput.Axis1D.PrimaryIndexTrigger : OVRInput.Axis1D.SecondaryIndexTrigger;

        // Hand geschlossen
        bool handClosed = (OVRInput.Get(theHand, OVRInput.Controller.Touch) == 1f);
        // Zeigefinger ausgestreckt
        bool indexFingerPointing = (OVRInput.Get(theFinger, OVRInput.Controller.Touch) == 0f);

        return (handClosed && indexFingerPointing);
    } // end IndexFingerPointing

} // end InfoPointer
