﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHand : MonoBehaviour {

	[SerializeField]
	private GameObject hand;
    private Snappable snappable;

	void Start () {
		
	}
	
	void Update () {
        if (this.hand == null) return;

        // folgt dem zugewiesen HandAnchor der OVRPlayerControllers
		this.transform.position = this.hand.transform.position;
		this.transform.rotation =  this.hand.transform.rotation;
	} // end Update

    private void OnTriggerEnter(Collider other) {
        // Trigger von Objekt in Hand aktivieren
		//Debug.Log("Tag: " + other.tag);
        if (other.tag == "SnappingPoint") {
            other.transform.parent.GetComponent<Snappable>();
		} else if (other.tag == "RocketPart") {
            this.snappable = other.GetComponent<Snappable>();
        }
		//Debug.Log(this.name + " hat " + this.snappable);
        if (this.snappable != null) this.snappable.ToggleTriggers(true);
    } // end OnTriggerEnter

    private void OnTriggerExit(Collider other) {
        // Trigger von Objekt in Hand deaktivieren
		if (other.tag == "SnappingPoint" || other.tag == "RocketPart") {
			if (this.snappable == null) return;
            this.snappable.ToggleTriggers(false);
			this.snappable = null;
			//Debug.Log("Snappable von " + this.name + " entfernt");
        }
    } // end OnTriggerExit

} // end FollowHand