﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelfSlot : MonoBehaviour {

    private GameObject item;
    private GameObject instantiatedItem;
    private Spin spin;
    private int instanceCounter = 0;
    [SerializeField]
    private float respawnTime = 2f;
    
	void Start () {
        this.spin = this.GetComponent<Spin>();
    } // end Start

    public void SetItem(GameObject i) {
        this.item = i;
        this.Spawn();
    } // end SetItem

    private void Spawn() {
        if (this.instantiatedItem != null) Destroy(this.instantiatedItem);

        this.instantiatedItem = Instantiate(item, this.transform);
        this.instantiatedItem.name = this.item.name + "-" + this.instanceCounter;
        this.instanceCounter++;
        //this.spin.Restart(this.instantiatedItem);
    } // end Spawn

    private IEnumerator RespawnItem() {
        yield return new WaitForSeconds(this.respawnTime);
        this.Spawn();
    } // end RespawnItem

    private void OnTriggerExit(Collider other) {
		if (this.instantiatedItem == null) return;
        if (other.transform.parent.name == this.instantiatedItem.name) {
            this.instantiatedItem.transform.SetParent(null);
            this.instantiatedItem = null;
            //this.spin.Stop();
            StartCoroutine("RespawnItem");
        }
    } // end OnTriggerExit

} // end ShelfSlot
