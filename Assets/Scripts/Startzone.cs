﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startzone : MonoBehaviour {

	public List<GameObject> currentlyColliding = new List<GameObject>();
    public GameObject statusLight;
    public Material matPositive;
    public Material matNegative;

	void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("RocketModel")) {
            this.currentlyColliding.Add(other.gameObject);
            this.statusLight.GetComponent<Renderer>().material = this.matPositive;
            //Debug.Log("Anzahl Elemente in Startzone: " + this.currentlyColliding.Count);
        }
    }

	void OnTriggerExit(Collider other) {
        this.currentlyColliding.Remove(other.gameObject);
        if (this.currentlyColliding.Count == 0) {
            this.statusLight.GetComponent<Renderer>().material = this.matNegative;
        }
	}

	public List<GameObject> GetRocketParts() {
		return this.currentlyColliding;
	}

}
