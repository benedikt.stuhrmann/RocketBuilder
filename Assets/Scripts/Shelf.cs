﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Shelf : MonoBehaviour {

    private List<Transform> slots = new List<Transform>();
    [SerializeField]
    private List<GameObject> items;

	void Start () {
        foreach (Transform child in GetComponentsInChildren<Transform>()) {
            if (child.tag == "ShelfFloor") {
                foreach (Transform slot in GetComponentsInChildren<Transform>()) {
                    if (slot.tag == "ShelfSlot") {
                        this.slots.Add(slot.GetComponent<Transform>());
                    }
                }
            }
        }

        StartCoroutine("LateStart");

    } // end Start

    private IEnumerator LateStart() {
        yield return new WaitForSeconds(0.5f);

        // Regal auffüllen
        int counter = -1;
        foreach (GameObject item in this.items) {
            counter++;

            if (item == null) continue;
            if (counter > this.slots.Count) break;

            slots[counter].GetComponent<ShelfSlot>().SetItem(item);
        }
    } // end LateStart
	
} // end Shelf
