﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

    private GameObject item;
    [SerializeField]
    private float rotationSpeed = 5f;
    private Vector3 direction;

	void Start () {
        this.direction = Vector3.up + Vector3.left;
	} // end Start

    public void Stop() {
        this.item = null;
    } // end Stop

    public void Restart(GameObject i) {
        //this.item = i;
        //this.item.transform.rotation = Random.rotation;
    } // end Restart
	
	void Update () {
        if (!this.item) return;
        this.item.transform.Rotate(this.direction, this.rotationSpeed * Time.deltaTime, Space.World);
    } // end Update
}
